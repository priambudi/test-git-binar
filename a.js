// input: Priambudi Lintang Bagaskara

// abbreviate("Priambudi Lintang Bagaskara")

// output: priambudi-lintang-bagaskara

// replaceAll('priambudi-lintang', 'a', '4') -> 'pri4mbudi-lint4ng'
function replaceAll(str, toReplace, char) {
    let res = ''
    for (let i=0; i<str.length; i++) {
        if (str[i] == toReplace) {
            res+=char
        } else {
            res+=str[i]
        }
    }
    return res
}
function abbreviate(str) {
    // str.split(' ') => "Bagas Priambudi" -> ["Bagas", "Priambudi"]
    // .map((s)=>s.toLowerCase()) => "Bagas".toLowerCase() dan "Priambudi".toLowerCase() -> ["bagas", "priambudi"]
    // .join('-') => ["bagas", "priambudi"] -> "bagas-priambudi"
    console.log(
        str.split(' ')
        .map((s)=>s.toLowerCase())
        .join('-')
    )
}
    
function abbr(str) {
    let store = '';
    for (let i=0; i<str.length; i++ ) {
        if (str[i-1] == ' ') {     
            store+= str[i].toLowerCase();
        } else if (i == 0) {        
            store+= str[i].toLowerCase()
        } else if (str[i] == " ") {
            store+= '-'
        } else {
            store += str[i];
        }
    }
    return store;
}

let t0 = new Date().getTime()
abbreviate("Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4 Pr14mbudi L1nt4ng 134g4sk4r4") // pr14mbudi-l1nt4ng-134g4sk4r4
let t1 = new Date().getTime()

console.log(`${t1-t0}ms`, t0, t1)