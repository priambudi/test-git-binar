const getMiddle = (l, r) => {
    return l + (r - l) / 2;
};

// arr1 = [1,2]
// arr2 = [3,4]
// return [1,2]
const merge = (arr1, arr2) => {
    let _arr1 = arr1;
    let _arr2 = arr2;
    let res = [];
    while (_arr1.length > 0 && _arr2.length > 0) {
        if (_arr1[0] < _arr2[0]) {
            res.push(_arr1.shift())
        } else {
            res.push(_arr2.shift())
        }
    }
    if (arr1.length === 0) {
        while (_arr2.length > 0) {
            res.push(_arr2.shift())
        }
    } else {
        while (_arr1.length > 0) {
            res.push(_arr1.shift())
        }
    }
    return res;
};

const mergeSort = (arr) => {
    const mergeSortHelper = (arr, l, r) => {
        if (l >= r - 1) {
            return arr.slice(l,r + 1);
        }
        const middle = getMiddle(l, r);
        const leftArr = mergeSortHelper(arr, 0, middle);
        const rightArr = mergeSortHelper(arr, middle+1, r);
        return merge(leftArr, rightArr);
    };
    return mergeSortHelper(arr, 0, arr.length - 1)
};

console.log(getMiddle(0, 8))
console.log(merge([1,3], [2,4]))
console.log(mergeSort([4, 5, 2, 7, 1, 8, 23, 12]))