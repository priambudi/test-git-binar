import { Suspense } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";

import NavBar from "./components/Navbar";
import routes from "./routes";

function App() {
  return (
    <>
      <Router>
        <NavBar />
        <Switch>
          {routes.map((route) => {
            return (
              <Route key={route.path} exact={route.exact} path={route.path}>
                <Suspense fallback={<div>Loading...</div>}>
                  {route.component}
                </Suspense>
              </Route>
            );
          })}
        </Switch>
      </Router>
    </>
  );
}

export default App;
