import { Component } from 'react';
import { Rating, Typography } from '@mui/material';
// PascalCase
class RatingComponentClassComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            starCount: this.props.stars,
            loading: true,
        };
    }

    handleCallApi = () => {
        this.setState({
            loading: true
        })
        setTimeout(() => {
            this.setState({
                loading: false,
                starCount: 5,
            })
        }, 3000)
    };

    handleCheckState = () => {
        console.log('starCount', this.state.starCount)
        if (this.state.starCount < 3) {
            alert('sad :(')
        }
    };

    componentDidMount() {
        // this.handleCallApi()
    }

    componentDidUpdate(prevProps, prevState) {
        // console.log('prevProps', prevProps)
        // console.log('currentProps', this.props)
        if (this.state.starCount !== prevState.starCount) {
            this.handleCheckState();
        }
    }

    render() {
        return <div>
            <Typography component="legend">{this.state.loading ? 'loading' : 'Controlled'}</Typography>
            <Rating
                name="simple-controlled"
                value={this.props.stars}
                onChange={(event, newValue) => {
                    console.log('onChange', newValue)
                    this.props.onStarsChange(newValue)
                }}
            />
        </div>
    }
}

export default RatingComponentClassComponent;