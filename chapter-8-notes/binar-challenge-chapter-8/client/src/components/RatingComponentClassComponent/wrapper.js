import { Component } from 'react';
import RatingComponentClassComponent from '.';
// PascalCase
class Wrapper extends Component {
    constructor(props) {
        super(props);
        this.state = {
            starCount: 3,
        };
    }

    handleClick = () => {
        if (this.state.starCount < 5) {
            this.setState({
                starCount: this.state.starCount + 1
            })
        } else {
            this.setState({
                starCount: 0
            })
        }
    }

    handleChange = (count) => {
        console.log('handleChange', count)
        this.setState({
            starCount: count
        })
    }

    render() {
        return <>
            <h1 onClick={this.handleClick}>hehe</h1>
            <RatingComponentClassComponent
                stars={this.state.starCount}
                onStarsChange={this.handleChange}
            />
        </>
    }
}

export default Wrapper;