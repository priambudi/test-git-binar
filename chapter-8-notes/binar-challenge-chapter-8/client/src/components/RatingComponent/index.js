import { useState, useEffect } from 'react';
import { Rating, Typography } from '@mui/material';
import { ProgressBar, Placeholder } from 'react-bootstrap';

// PascalCase
const RatingComponent = (props) => {
    const { stars } = props;
    const [starCount, setStarCount] = useState(stars);
    const [loading,] = useState(false);

    // const handleCallApi = () => {
    //     setLoading(true)
    //     setTimeout(() => {
    //         setStarCount(5)
    //         setLoading(false)
    //     }, 3000)
    // };

    const handleCheckState = () => {
        console.log('starCount', starCount)
    };

    // useEffect(handleCallApi, [])
    useEffect(handleCheckState, [starCount])

    const renderRating = () => {
        if (loading) {
            return <Placeholder xs={1} />
        } else {
            return <Rating
                name="simple-controlled"
                value={starCount}
                onChange={(event, newValue) => {
                    setStarCount(newValue);
                }}
            />
        }
    }

    return <div>
        <Typography component="legend">{loading ? 'loading' : 'Controlled'}</Typography>
        {renderRating()}
        <ProgressBar
            now={starCount}
            label={`${(starCount / 5) * 100}%`}
            min={0}
            max={5}
        />
        <div className="progress">
            <div
                className="progress-bar"
                role="progressbar"
                style={{ width: `${(starCount / 5) * 100}%` }}
                aria-valuenow={starCount}
                aria-valuemin={0}
                aria-valuemax={5}
            >
                {`${(starCount / 5) * 100}%`}
            </div>
        </div>
    </div>
}

export default RatingComponent;