import { Container, Nav, Navbar, NavDropdown } from 'react-bootstrap';
import { Link } from 'react-router-dom';
// PascalCase
const NavBar = () => {
    return <Navbar bg="light" expand="lg">
        <Container>
            <Navbar.Brand href="#home" className="test">React-Bootstrap</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="me-auto">
                    <Nav.Link><Link style={{ textDecoration: 'none' }} to="/">Home</Link></Nav.Link>
                    <NavDropdown title="Dropdown" id="basic-nav-dropdown">
                        <NavDropdown.Item><Link style={{ textDecoration: 'none' }} to="/rating">Rating Functional</Link></NavDropdown.Item>
                        <NavDropdown.Item><Link style={{ textDecoration: 'none' }} to="/rating-class">Rating Class</Link></NavDropdown.Item>
                        <NavDropdown.Item><Link style={{ textDecoration: 'none' }} to="/suit">Suit</Link></NavDropdown.Item>
                        <NavDropdown.Item><Link style={{ textDecoration: 'none' }} to="/counter">Counter</Link></NavDropdown.Item>
                        <NavDropdown.Divider />
                        <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
                    </NavDropdown>
                </Nav>
            </Navbar.Collapse>
        </Container>
    </Navbar>
}

export default NavBar;