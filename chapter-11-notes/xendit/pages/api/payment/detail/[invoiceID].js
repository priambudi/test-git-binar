// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
const xendit = require("xendit-node");
export default async function handler(req, res) {
  const { invoiceID } = req.query;
  const { Invoice } = new xendit({
    secretKey:
      "xnd_development_tPq5Qm70YnKsPUK7zQoBC1Z2HxxIjoQPZ7FCLpxD2oW9zrKS9qZauOwFcoPlFL",
  });

  const invoiceSpecificOptions = {};
  const i = new Invoice(invoiceSpecificOptions);

  const resp = await i.getInvoice({
    invoiceID,
  });
  console.log(resp);
  res.status(200).json({ name: "John Doe", data: resp });
}
