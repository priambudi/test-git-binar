var fs = require('fs');
var stream = fs.createWriteStream("data.txt");
stream.once('open', function(fd) {
  for (let i = 0; i < 100000; i++) {
    stream.write(`SET ${i} ${i}\n`);
  }
  stream.end();
});