## Quick Start

Clone Repo:

```bash
git clone https://gitlab.com/priambudi/test-git-binar.git
```

Change Directory:

```bash
cd test-git-binar/chapter-5-notes/express-clean
```

Install Dependencies:

```bash
# with NPM
npm install

# with Yarn
yarn
```

Start the server:

```bash
# with NPM
npm start

# with Yarn
yarn start
```

View the website at: http://localhost:3000
