module.exports = {
  success: (message) => JSON.stringify({
    status: 'success',
    message,
  }),
  fail: (message) => JSON.stringify({
    status: 'fail',
    message,
  }),
};
