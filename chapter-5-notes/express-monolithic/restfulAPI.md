# method
GET     http://localhost:5000/api/user/1
balikin data user dengan ID 1 aja

PUT     http://localhost:5000/api/user/1
masukin data baru dengan ID 1

PATCH   http://localhost:5000/api/user/1
update data user dengan ID 1

DELETE  http://localhost:5000/api/user/1
hapus data user dengan ID 1

# status
menggambarkan status dari komunikasi API. Apakah sukses? url-nya dialihkan? ada kesalahan dari request (sisi client)? ada kesalahan dari sisi response (server)?

# request header & body
untuk menampung data seperti data form login, data form register.

# endpoint
url dari API yang kita buat.
contoh: http://localhost:5000/api/user/1

# contoh

API untuk update data user (nama user) yang sudah ada dengan ID 1

REQUEST

PATCH   http://localhost:5000/api/user/1

request header: {
    ...
    Accept-Encoding: gzip, deflate, br
    Host: localhost:5000,
    dst ...
}

request body: {
    nama: "Bagas 2"
}

Penjelasan:
1. Endpoint:    http://localhost:5000/api/user/1
2. Method:      PATCH
3. request header: {
    ...
    Accept-Encoding: gzip, deflate, br
    Host: localhost:5000,
    dst ...
}
4. request body: {
    nama: "Bagas 2"
}