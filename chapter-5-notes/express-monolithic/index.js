const express = require('express');
const morgan = require('morgan');
const compression = require('compression');
require('dotenv').config();

const app = express();
const port = process.env.API_GATEWAY_PORT || 3000;
const users = [
  { email: 'a@a.com', password: 'a' },
];

app.set('view engine', 'ejs');
// application level middleware
// app.use(logMiddleware)
// app.use(timeMiddleware)

// built-in middleware
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// route level middleware
const router = require('./router');

app.use('/api', router);

// third party middleware
app.use(morgan('dev'));
app.use(compression());

app.get('/', (req, res) => {
  const userLength = users.length;
  res.render('index', {
    name: userLength,
    title: 'mr',
  });
  // res.set('Content-Type', 'text/html');
  // res.send(Buffer.from(`<h2>Test String: ${name}</h2>`))
});
app.get('/register', (req, res) => {
  res.render('register');
});
app.post('/register', (req, res) => {
  const { email, password } = req.body;
  if (email === process.env.BLACKLIST_EMAIL) {
    res.status(400).json({
      status: 'error',
      message: 'email is blacklisted',
    });
  }
  users.push({ email, password });
  console.log(users);
  res.redirect('/');
//   res.status(200).json({
//     status: 'success',
//     message: 'success.message',
//   });
});

app.get('/login', (req, res) => {
  res.render('login');
});
app.post('/login', (req, res) => {
  const { email, password } = req.body;
  // cek ada gak email itu di array users
  const obj = users.find((u) => u.email === email);
  if (obj) {
    if (obj.password === password) {
      res.redirect('/');
    }
  }
  console.log(users, email, password);
  res.status(200).json({
    status: 'fail',
    message: 'gabisa login',
  });
});

app.get('/user/:email', (req, res) => {
  const { email } = req.params;
  let user;
  if (email) {
    user = users.find((u) => u.email === email);
  } else {
    user = users;
  }

  if (user) {
    res.status(200).json({
      status: 'success',
      message: user,
    });
  }
  res.status(200).json({
    status: 'fail',
    message: 'no user with that email',
  });
});

// error handling middleware - paling bawah!!!
const handleErrorMiddleware = (err, req, res) => {
  console.log(err.message);
  // res.status(500);
  // .json({
  //   status: 'fail',
  //   errors: err.message,
  // });
};
app.use(handleErrorMiddleware);

app.listen(port, () => console.log(`App running at port ${port}`));
