const add = (a) => (b) => a + b;
const add5 = add(5);
const add100 = add(100);

console.log(add5(10)); // 15
console.log(add100(10)); // 110

const tambah = (a, b) => a + b;
const tambah5 = (a) => 5 + a;
const tambah100 = (a) => 100 + a;

console.log(tambah5(10)); // 15
console.log(tambah100(10)); // 110

const mul = (...args) => args.reduce((acc, curr) => acc * curr);
console.log(mul(1, 2, 3, 4));
