const express = require('express');
const path = require('path');
const USER_DATA = require('./data.json');
const response = require('./response');

const router = express.Router();

const timeAndBasicLogMiddleware = (req, res, next) => {
  const date = new Date();
  console.log(`timeAndBasicLogMiddleware - ${req.method} ${req.url}`, date);
  next();
};
const handlePing = (req, res) => {
  res.send(response.success('Server up and running on 3000'));
};
router.use(timeAndBasicLogMiddleware);

router.use(express.static(path.join(__dirname, 'static')));

router.get('/', (req, res) => {
  res.sendFile('./static/index.html', { root: __dirname });
});
router.get('/game', (req, res) => {
  res.sendFile('./static/index.html', { root: __dirname });
});

router.get('/user/all', (req, res) => {
  res.send(response.success(USER_DATA));
});
router.get('/user/all/married', (req, res) => {
  const married = USER_DATA.filter((item) => item.is_married === true);
  res.send(response.success(married));
});
router.get('/user/all/single', (req, res) => {
  const married = USER_DATA.filter((item) => item.is_married === false);
  res.send(response.success(married));
});
router.get('/user/all/size', (req, res) => {
  const [s, m, l] = [[], [], []];
  USER_DATA.forEach((item) => {
    if (item.shirt_size === 'S') s.push(item);
    if (item.shirt_size === 'M') m.push(item);
    if (item.shirt_size === 'L') l.push(item);
  });
  res.send(response.success({
    S: s,
    M: m,
    L: l,
  }));
});
router.get('/user/:id', (req, res) => {
  const { id } = req.params;
  res.send(response.success(USER_DATA.filter((user) => user.id === parseInt(id))));
});
router.get('/user/gender/:gender', (req, res) => {
  const { gender } = req.params;
  res.send(response.success(USER_DATA.filter((user) => user.gender === gender)));
});
router.post('/ping', handlePing);
module.exports = router;
