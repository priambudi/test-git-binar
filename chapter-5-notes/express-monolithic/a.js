const add = (a, b) => a + b;
console.log(add(1, 6));

// eslint-disable-next-line arrow-body-style
const add1 = (a, b) => {
  return a + b;
};
console.log(add1(1, 6));

const add2 = function (a, b) {
  return a + b;
};
console.log(add2(1, 6));

function add3(a, b) {
  return a + b;
}
console.log(add3(1, 6));

const add4 = (a) => (b) => a + b;
console.log(add4(1)(6));
