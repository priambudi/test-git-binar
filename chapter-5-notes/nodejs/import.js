function add(a,b){
    return a+b
}

// multiply
function mul(a,b){
    return a*b
}

// export object
module.exports = {
    keyAdd: add,
    keyMul: mul
}
