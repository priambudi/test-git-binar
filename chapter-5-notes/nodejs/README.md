tutorial menjalankan aplikasi ini

```
# clone repo
git clone https://gitlab.com/priambudi/test-git-binar.git

# change directory
cd test-git-binar/chapter-5-notes/nodejs

# install packages
(npm) npm install
(yarn) yarn

# start
(npm) npm run start
(yarn) yarn run start
```