// cara 1
var {keyAdd, keyMul} = require('./import.js');
const os = require('os')
const fs = require('fs')
// cara 2
// var math = require('./import.js');
// let keyAdd = math.keyAdd;
// let keyMul = math.keyMul;

const isiTxt = fs.readFileSync('./index.txt', 'utf-8')
const isiHtml = fs.readFileSync('./index.html', 'utf-8')
const isiJs = fs.readFileSync('./import.js', 'utf-8')
console.log(`Free memory: ${os.freemem()}`)
console.log(isiTxt)
fs.writeFileSync('./new-index.txt', 'binar, i love')
const isiTxt2 = fs.readFileSync('./new-index.txt', 'utf-8')
console.log(isiTxt2)
// console.log(isiHtml)
// console.log(isiJs)
// console.log(keyAdd(1,4))
// console.log(keyMul(1,4))

const data = require('./data.json');
// console.log(data)
// console.log(data.data)
// console.log(data.data.name)

const createPerson = function(person) {
    fs.writeFileSync('./person.json', JSON.stringify(person))
    return person;
}
const bagasObj = {
    error: null,
    code: 200,
    data: {
        name: "Bagas tapi dari javascript",
        age: 21,
        isMarried: false,
        hobby: ["gaming", "sleeping"],
        school: [
            {
                city: "BSD",
                id: 1
            },
            {
                city: "Depok",
                id: 2
            }
        ]
    }
}
const bagas = createPerson(bagasObj)
console.log(bagas)

// cara update json:
// apus data.json
// di vscode, klik kanan data.json, pilih "Copy Path"
fs.unlinkSync("/Users/priambudi/Documents/hehe/binar/test-git-binar/chapter 5 notes/nodejs/data.json");
// bikin data.json baru dengan data yang terupdate
// ceritanya hobby ini unique
if (!data.data.hobby.includes('swimming')) {
    data.data.hobby.push('swimming')
}

// id incremental & unique
data.data.school.push({
    city: "Jakarta",
    id: data.data.school.length + 1
})

// aku suka sama x
// cari x, ganti jadi (input)
fs.writeFileSync('./data.json', JSON.stringify(data))

console.log("hello world dari golang")