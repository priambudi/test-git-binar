const express = require('express')
const app = express()
const port = 3004

app.get('/home', (req, res) => {
    const data = {
        error: null,
        message: `dari server home 3004 yay`,
    }
    res.send(JSON.stringify(data))
})
app.get('/ping', (req, res) => {
    res.send('pong')
})
app.listen(port, () => console.log(`Service Home running!`))