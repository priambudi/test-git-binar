# Guide

## Requirements
1. [Node JS](https://nodejs.org/en/)
2. [Golang](https://golang.org/doc/install) (optional)

## Clone Repo
```
git clone https://gitlab.com/priambudi/test-git-binar.git
```

## Change Directory
```
cd test-git-binar/chapter-5-notes/express-microservice
```

## Install Dependencies
with NPM
```
npm install
```
or, with Yarn
```
yarn
```

## Run Project
with NPM
```
npm start
```
or, with Yarn
```
yarn start
```


## URLs
http://localhost:3000/home

http://localhost:3000/golang