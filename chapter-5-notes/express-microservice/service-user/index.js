const express = require('express')
const morgan = require('morgan')
var compression = require("compression")
const app = express()
const port = 3003

// third party middleware
app.use(morgan('dev'))
app.use(compression())

app.get('/user', (req, res) => {
    res.status(200).json({
        status: 'success',
        message: "bagas"
    })
})

app.get('/ping', (req, res) => {
    res.status(200).json('pong')
})

app.listen(port, () => console.log(`Service user is running!`))