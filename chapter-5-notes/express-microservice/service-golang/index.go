package main

import (
    "fmt"
    "net/http"
)

func ping(w http.ResponseWriter, req *http.Request) {
    fmt.Fprintf(w, "pong\n")
}

func hello(w http.ResponseWriter, req *http.Request) {
    fmt.Println("hello from golang")
    fmt.Fprintf(w, "hello from golang!\n")
}

func logs(w http.ResponseWriter, req *http.Request) {
    fmt.Fprintf(w, "Error req: %s %s\n", req.Host, req.URL.Path)
    fmt.Printf("Error req: %s %s\n", req.Host, req.URL.Path) 
}

func main() {
    http.HandleFunc("/ping/", ping)
    http.HandleFunc("/golang/", hello)
    http.HandleFunc("/", logs)
    fmt.Println("Service Golang is running!")
    http.ListenAndServe(":3002", nil)
}