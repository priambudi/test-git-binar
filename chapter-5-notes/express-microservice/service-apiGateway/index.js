var express = require('express');
var app = module.exports = express();
const port = 3000;
var proxy = require('http-proxy').createProxyServer({
    host: `http://localhost:${port}`,
    // port: 3000
});
app.use('/golang', function(req, res, next) {
    proxy.web(req, res, {
        target: 'http://localhost:3002/golang'
    }, next);
});
app.use('/home', function(req, res, next) {
    const random = Math.random()
    if (random < 0.5) {
        try {
            proxy.web(req, res, {
                target: 'http://localhost:3001/home'
            }, next);
        } catch(e) {
            proxy.web(req, res, {
                target: 'http://localhost:3004/home'
            }, next);
        }
    } else {
        try {
            proxy.web(req, res, {
                target: 'http://localhost:3004/home'
            }, next);
        } catch(e) {
            proxy.web(req, res, {
                target: 'http://localhost:3001/home'
            }, next);
        }
    }
});
app.use('/user', function(req, res, next) {
    proxy.web(req, res, {
        target: 'http://localhost:3003/user'
    }, next);
});
app.use('/ping', (req, res) => {
    res.send('pong')
})

app.listen(3000, function(){
    console.log(`Service Api Gateway running at port ${port}`)
});