const http = require("http");
const fs = require("fs");

function onRequest(request, response) {
    const url = request.url;
    if (url ==='/'){
        response.writeHead(200, {"Content-Type": "text/html"})
        fs.readFile("./index.html", null, (error, data) => {
            if (error) {
                response.writeHead(404);
                response.write("file not found")
            } else {
                response.write(data)
            }
            response.end()
        })
     } else if(url ==='/contact'){
        response.write('<h1>contact us page<h1>');
        response.end();
     } else if(url ==='/ping'){
        response.writeHead(200, {"Content-Type": "application/json"})
        const data = {
            error: null,
            message: `Server up and running on ${request.headers.host}`,
        }
        response.end(JSON.stringify(data));
     } else if (request.method === 'GET'){
        response.writeHead(404);
        response.write("file not found")
        response.end()
     }
}

http.createServer(onRequest).listen(8000)