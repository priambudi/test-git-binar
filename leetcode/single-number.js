// https://leetcode.com/problems/single-number/

var singleNumber = function(nums) {
    let cache = {};
    for (let i = 0; i < nums.length; i++) {
        if (cache[nums[i]]) {
            delete cache[nums[i]]
        } else {
            cache[nums[i]] = 1
        }
    }
    return Object.keys(cache)[0]
};

console.log(singleNumber([1,1,2,2,3,4,4]))