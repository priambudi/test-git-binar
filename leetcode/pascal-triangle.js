// https://leetcode.com/problems/pascals-triangle/

/**
 * @param {number} numRows
 * @return {number[][]}
 */
 var generate = function(numRows) {
     let temp = []
    for (let i = 1; i <= numRows; i++) {
        if (i - 1 > 1) {
            let _temp = []
            for (let j = 0; j < i; j++) {
                if (j === 0 || j === i - 1) {
                    _temp.push(1)
                } else {
                    const prevPascal = temp[i - 2]
                    let count = prevPascal[j - 1] + prevPascal[j]
                    _temp.push(count);
                }
            }
            temp.push(_temp)
        } else if (i === 1) {
            temp.push([1])
        } else if (i === 2) {
            temp.push([1, 1])
        }
    }
    return temp
};

// ver. 2
// var generate = function(numRows) {
//     var pascal = [];
//     for (var i = 0; i < numRows; i++) {
//         pascal[i] = [];
//         pascal[i][0] = 1;
//         for (var j = 1; j < i; j++) {
//             pascal[i][j] = pascal[i-1][j-1] + pascal[i-1][j]
//         }
//         pascal[i][i] = 1;
//     }
//     return pascal;
// };

console.log(generate(10))