// npm install -g typescript
// tsc -v
// tsc typescript
// node typescript

interface Addition {
    firstNum: number
    secondNum: number
}

const add = ({firstNum,secondNum}: Addition) => {
    return firstNum + secondNum
};

console.log(add({firstNum: 1, secondNum: 2}));