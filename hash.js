const str = 'bAgas';
const hash = (st, salt = 2) => {
    let result = '';
    st.split('').forEach(char => {
        let currentCharASCII = char.charCodeAt(0);
        result += String.fromCharCode(currentCharASCII + salt)
    })
    return result;
};

const deHash = (st, salt = 2) => {
    let result = '';
    st.split('').forEach(char => {
        let currentCharASCII = char.charCodeAt(0);
        result += String.fromCharCode(currentCharASCII - salt)
    })
    return result;
};
console.log(hash(str,2))
console.log(deHash(hash(str,2)))