import axios from "axios";

const normalAxiosGet = (url, onSuccess, onError) => {
  return axios
    .get(url)
    .then(onSuccess)
    .catch(onError);
};

export default normalAxiosGet;
