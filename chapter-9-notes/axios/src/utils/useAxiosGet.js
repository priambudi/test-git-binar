import axios from "axios";
import { useState } from "react";

const useAxiosGet = (url) => {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const getData = () =>
    axios
      .get(url)
      .then(function (response) {
        setData(response.data);
        setIsLoading(false);
      })
      .catch(function (error) {
        setData(null);
        setIsLoading(false);
      });
  return {
    data: data,
    isLoading: isLoading,
    getData: getData
  };
};

export default useAxiosGet;