const data = [
    {
        1: 1,
        3: 0
    },
    {
        1: 0,
        3: 1
    },
    {
        1: 1,
        3: 0
    }
]

const parse = (data) => {
    const res = {};
    data.forEach((item) => {
        Object.keys(item).map(key => {
            if (!res[key]) {
                res[key] = item[key]
            } else {
                res[key] += item[key]
            }
        })
    })
    return res;
}

const expected = {
    1: 2,
    3: 1
}

console.log(parse(data))