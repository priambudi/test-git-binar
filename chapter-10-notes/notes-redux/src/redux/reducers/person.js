import { insertItemToArray } from "../../utils";
import { ADD, REMOVE } from "../actions/person";

const initialState = [
  {
    id: 1,
    name: "Bagas",
  },
  {
    id: 2,
    name: "Bagus",
  },
  {
    id: 3,
    name: "Agas",
  },
];

function person(state = initialState, action) {
  console.log(action.payload);
  switch (action.type) {
    case ADD:
      return insertItemToArray(action.payload, state);
    case REMOVE:
      return state.filter((item) => item.id !== action.payload);
    default:
      return state;
  }
}

export default person;
