import { insertItemToArray } from "../../utils";
import { ADD, REMOVE } from "../actions/game";

function game(state = [], action) {
    console.log(action.payload)
  switch (action.type) {
    case ADD:
      return insertItemToArray(action.payload, state);
    case REMOVE:
        return state.filter(item => item.id !== action.payload);
    default:
      return state;
  }
}

export default game;