import { combineReducers } from "redux";
import persons from "./person";
import games from "./game";

export default combineReducers({
  persons,
  games,
});
