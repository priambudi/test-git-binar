import logo from "./logo.svg";
import "./App.css";
import { Component, useEffect } from "react";
import { connect, useDispatch } from "react-redux";
import { ADD, REMOVE } from "./redux/actions/person";
import { ADD as ADD_GAME, REMOVE as REMOVE_GAME } from "./redux/actions/game";

const mapStateToProps = (state) => ({
  state: state,
  dataPersons: state.persons,
  dataGames: state.games,
});
class ClassComponent extends Component {
  componentDidMount() {
    console.log("Data Redux Store: ", this.props.state);
  }

  render() {
    const { state, dataPersons, dataGames, dispatch } = this.props;
    console.log("state", state);
    console.log("dataPersons", dataPersons);
    console.log("dataGames", dataGames);
    return (
      <>
        <h1>Class Component</h1>
        {dataPersons.map((item) => (
          <div
            key={item.id}
            onClick={() => {
              dispatch({
                type: REMOVE,
                payload: item.id,
              });
            }}
          >
            {item.name}
          </div>
        ))}
        {dataGames.map((item) => (
          <div
            key={item.id}
            onClick={() => {
              dispatch({
                type: REMOVE_GAME,
                payload: item.id,
              });
            }}
          >
            {item.name}
          </div>
        ))}
      </>
    );
  }
}
const ConnectedClassComponent = connect(mapStateToProps)(ClassComponent);

function FunctionalComponent(props) {
  const dispatch = useDispatch();
  useEffect(() => {
    console.log("Data Redux Store: ", props.data);
  }, []);
  const { state, dataPersons, dataGames } = props;
  return (
    <>
      <h1>Functional Component</h1>
      {dataPersons.map((item) => (
        <div
          key={item.id}
          onClick={() => {
            dispatch({
              type: REMOVE,
              payload: item.id,
            });
          }}
        >
          {item.name}
        </div>
      ))}

      {dataGames.map((item) => (
        <div
          key={item.id}
          onClick={() => {
            dispatch({
              type: REMOVE_GAME,
              payload: item.id,
            });
          }}
        >
          {item.name}
        </div>
      ))}
    </>
  );
}
const ConnectedFunctionalComponent =
  connect(mapStateToProps)(FunctionalComponent);

function App() {
  const dispatch = useDispatch();
  const handleClickPerson = () => {
    dispatch({
      type: ADD,
      payload: {
        id: new Date().getMilliseconds(),
        name: "Bagas " + new Date().getMilliseconds(),
      },
    });
  };
  const handleClickGame = () => {
    dispatch({
      type: ADD_GAME,
      payload: {
        id: new Date().getMilliseconds(),
        name: "Dota " + new Date().getMilliseconds(),
      },
    });
  };
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <ConnectedClassComponent />
        <ConnectedFunctionalComponent />
        <button onClick={handleClickPerson}>Add Person</button>
        <button onClick={handleClickGame}>Add Game</button>
      </header>
    </div>
  );
}

export default App;
