const express = require('express');
const redis = require("redis");
const Sequelize = require('sequelize');
const { QueryTypes } = require('sequelize');
require("dotenv").config();
const client = redis.createClient();
// 'postgres://user:pass@host:port/dbname'
const sequelize = new Sequelize(
  `postgres://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`
);
sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

// sequelize manual query
// https://sequelize.org/master/manual/raw-queries.html
// const test = await sequelize.query("SELECT * FROM fibonaccis", {
//   type: QueryTypes.SELECT
// });
// console.log(test)

const Fibonacci = sequelize.define('fibonacci', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true
  },
  value: {
    type: Sequelize.STRING
  }
});

// force: true will drop the table if it already exists
Fibonacci.sync({ force: true }).then(() => {
  // seed 100000 data
  // for (let i = 0; i < 100000; i++) {
  //   Fibonacci.create({
  //     id: i.toString(),
  //     value: i.toString()
  //   });
  // }
  // Table created
  return true
});

client.on("error", function (error) {
  console.error(error);
});

const app = express();
const port = process.env.API_GATEWAY_PORT || 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

function fib(num) {
  // base case (kasus di mana rekursif berhenti/di-return)
  if (num === 2 || num === 1) {
    return 1
  }
  else return fib(num - 1) + fib(num - 2)
}

function fibCache(num, cache = {}) {
  // cache di javascript -> object
  // kalo bahasa lain -> HashMap/HashTable
  let _cache = cache
  if (_cache[num] !== undefined) {
    return _cache[num]
  }
  if (num === 2 || num === 1) {
    return 1
  }
  let res = fibCache(num - 1, _cache) + fibCache(num - 2, _cache)
  _cache[num] = res
  return res
}

app.get('/postgres/:num', (req, res) => {
  const { num } = req.params;
  Fibonacci.findOne({
    where: {
      id: num
    },
    raw: true,
  }).then(async users => {
    console.log(users)
    if (users) {
      res.set('Content-Type', 'text/html');
      res.send(Buffer.from(`<h2>Postgres: ${users.value}</h2>`))
    } else {
      let fibRes = fibCache(parseInt(num))
      Fibonacci.create({
        id: num,
        value: fibRes
      });
      res.set('Content-Type', 'text/html');
      res.send(Buffer.from(`<h2>Postgres manual: ${fibRes}</h2>`))
    }
  }).catch(e => {
    console.log(e)
    res.set('Content-Type', 'text/html');
    res.send(Buffer.from(`<h2>Postgres error: ${e}</h2>`))
  })
  
})
app.get('/delete/redis', (req, res) => {
  client.flushdb(function (err, succeeded) {
    console.log(succeeded); // will be true if successfull
    res.set('Content-Type', 'text/html');
    res.send(Buffer.from(`<h2>Delete Redis: ${succeeded}</h2>`))
  });
})
app.get('/redis/:num', (req, res) => {
  const { num } = req.params;
  // kalo ada di cache, kasih yang di cache
  client.get(num, function (err, reply) {
    // reply is null when the key is missing
    console.log(reply)
    if (reply != null) {
      res.set('Content-Type', 'text/html');
      res.send(Buffer.from(`<h2>Redis: ${reply}</h2>`))
    } else {
      // kalo gaada di cache, lakukan komputasi lalu kasih hasilnya
      const fibRes = fibCache(parseInt(num))
      client.set(num, fibRes);
      res.set('Content-Type', 'text/html');
      res.send(Buffer.from(`<h2>Redis manual: ${fibRes}</h2>`))
    }
  });
});

app.listen(port, () => console.log(`App running at port ${port}`));
