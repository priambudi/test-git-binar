'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('user_games', [
      {
        id: 1,
        username: 'test1',
        password: 'password',
        createdAt: '2017-08-09 07:00:00 -7:00',
        updatedAt: '2017-08-09 07:00:00 -7:00',
      },
      {
        id: 2,
        username: 'test2',
        password: 'password2',
        createdAt: '2017-08-09 07:00:00 -7:00',
        updatedAt: '2017-08-09 07:00:00 -7:00',
      },
      {
        id: 3,
        username: 'test3',
        password: 'password3',
        createdAt: '2017-08-09 07:00:00 -7:00',
        updatedAt: '2017-08-09 07:00:00 -7:00',
      }
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('user_games', null, {});
  }
};
