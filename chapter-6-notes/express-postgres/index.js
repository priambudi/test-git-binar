const express = require('express');
const Sequelize = require('sequelize');
const { QueryTypes } = require('sequelize');
require("dotenv").config();
const path = require("path");
const response = require('./response');

// 'postgres://user:pass@host:port/dbname'
const sequelize = new Sequelize(
  `postgres://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`
);
sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });


const app = express();
const port = process.env.API_GATEWAY_PORT || 5000;

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.set("view engine", "ejs");
app.set('views', path.join(__dirname, '/views'));

const UserGame = sequelize.define('user_games', {
  id: {
    type: Sequelize.BIGINT,
    primaryKey: true,
    autoIncrement: true
  },
  username: {
    type: Sequelize.STRING
  },
  password: {
    type: Sequelize.STRING
  }
});

// force: true will drop the table if it already exists
UserGame.sync({ force: false }).then(() => {
  return true
});

const handleCreateUser = async (req, res) => {
  const { username, password } = req.body;
  console.log(req.body)
  const user = await UserGame.create({
    username,
    password
  });
  if (user) {
    response.success(res, `Created new user.`);
  } else {
    response.fail(res, `Failed to create new user.`);
  }
}

const handleDeleteUser = (req, res) => {
  const { userID } = req.body;
  console.log(req.body)
  UserGame.destroy({
    where: {
      id: userID
    }
  })
    .then(async success => {
      if (success) {
        response.success(res, `Deleted user with id ${userID}.`);
      } else {
        response.fail(res, `No user with id ${userID}.`);
      }
    })
    .catch(e => {
      console.log(e)
      res.send(500)
    })
}

const handleEditUser = (req, res) => {
  const { userID, username, password } = req.body;
  UserGame.update(
    { username, password },
    { where: { id: userID } }
  )
    .then(([result]) =>{
      if (result) return response.success(res, `Edited user with id ${userID}.`)
      else return response.fail(res, `No user with id ${userID}.`)
    })
    .catch(err =>
      response.serverError(res, "something went wrong " + err)
    )
}

app.get('/', (req, res) => {
  UserGame.findAll({
    raw: true,
    order: [
      ['id', 'ASC'],
  ],
  })
    .then(users => {
      res.render("index", {
        users
      });
    })
})

// teknik merubah kodingan tanpa merubah flow aplikasi = Refactoring
// extract function to variable
app.get('/user/:userID', (req, res) => {
  const { userID } = req.params;
  UserGame.findOne({
    where: {
      id: userID
    },
    raw: true,
  })
    .then(user => {
      if (user) {
        res.render("detail", {
          user
        });
      } else {
        res.render("error", {
          message: `No user with id ${userID}.`
        });
      }
    })
    .catch(e => {
      res.send(500)
    })
})

app.get('/api/user', (req, res) => {
  UserGame.findAll({
    raw: true,
  })
    .then(users => {
      if (users) {
        response.success(res, users);
      } else {
        response.fail(res, `No data.`);
      }
    })
})

app.get('/api/user/:userID', (req, res) => {
  const { userID } = req.params;
  UserGame.findOne({
    where: {
      id: userID
    },
    raw: true,
  })
    .then(user => {
      if (user) {
        response.success(res, user);
      } else {
        response.fail(res, `No user with id ${userID}.`);
      }
    })
    .catch(e => {
      res.send(500)
    })
})

app.post('/api/user/create', handleCreateUser)
app.post('/api/user', handleCreateUser)

app.post('/api/user/delete', handleDeleteUser)
app.delete('/api/user', handleDeleteUser)

app.patch('/api/user', handleEditUser)

app.listen(port, () => console.log(`App running at port ${port}`));
