const express = require('express');
const { UserGame, UserGameBiodata } = require('./models')
require("dotenv").config();
const path = require("path");
const response = require('./response');
const { handleCreateUser, handleDeleteUser, handleEditUser } = require('./handlers/api');

const app = express();
const port = process.env.API_GATEWAY_PORT || 5000;

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.set("view engine", "ejs");
app.set('views', path.join(__dirname, '/views'));

app.get('/', (req, res) => {
  UserGame.findAll({
    raw: true,
    order: [
      ['id', 'ASC'],
    ],
    include: {
      model: UserGameBiodata
    }
  })
    .then(users => {
      res.render("index", {
        users
      });
    })
})

// teknik merubah kodingan tanpa merubah flow aplikasi = Refactoring
// extract function to variable
app.get('/user/:userID', (req, res) => {
  const { userID } = req.params;
  UserGame.findOne({
    where: {
      id: userID
    },
    raw: true,
  })
    .then(user => {
      if (user) {
        res.render("detail", {
          user
        });
      } else {
        res.render("error", {
          message: `No user with id ${userID}.`
        });
      }
    })
    .catch(e => {
      res.send(500)
    })
})

app.get('/api/user', (req, res) => {
  UserGame.findAll({
    raw: true,
  })
    .then(users => {
      if (users) {
        response.success(res, users);
      } else {
        response.fail(res, `No data.`);
      }
    })
})

app.get('/api/user/:userID', (req, res) => {
  const { userID } = req.params;
  UserGame.findOne({
    where: {
      id: userID
    },
    raw: true,
  })
    .then(user => {
      if (user) {
        response.success(res, user);
      } else {
        response.fail(res, `No user with id ${userID}.`);
      }
    })
    .catch(e => {
      res.send(500)
    })
})

app.post('/api/user/create', handleCreateUser)
app.post('/api/user', handleCreateUser)

app.post('/api/user/delete', handleDeleteUser)
app.delete('/api/user', handleDeleteUser)

app.patch('/api/user', handleEditUser)

app.listen(port, () => console.log(`App running at port ${port}`));
