const { UserGame, UserGameBiodata } = require("../models");
const response = require("../response");
module.exports = {
    handleCreateUser: async (req, res) => {
        const { username, password } = req.body;
        const user = await UserGame.create({
            username,
            password
        });
        if (!user) {
            response.fail(res, `Failed to create new user.`);
            return;
        }

        const userGameBiodata = await UserGameBiodata.create({
            fullname: "hehe",
            biodata: "hahaha",
            user_id: user.id
        });
        if (!userGameBiodata) {
            response.fail(res, `Failed to create new user biodata.`);
            return;
        }

        response.success(res, `Created new user.`);
    },
    handleDeleteUser: (req, res) => {
        const { userID } = req.body;
        UserGame.destroy({
            where: {
                id: userID
            }
        })
            .then(async success => {
                if (success) {
                    response.success(res, `Deleted user with id ${userID}.`);
                } else {
                    response.fail(res, `No user with id ${userID}.`);
                }
            })
            .catch(e => {
                console.log(e)
                res.send(500)
            })
    },
    handleEditUser: (req, res) => {
        const { userID, username, password } = req.body;
        UserGame.update(
            { username, password },
            { where: { id: userID } }
        )
            .then(([result]) => {
                if (result) return response.success(res, `Edited user with id ${userID}.`)
                else return response.fail(res, `No user with id ${userID}.`)
            })
            .catch(err =>
                response.serverError(res, "something went wrong " + err)
            )
    }
};
