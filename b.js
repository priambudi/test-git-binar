// fibonacci angka ke i = i-1 + i-2
// 1 1 2 3 5 8 13
// input: fib(7)
// output: 13

const handlerMath = require("./c")

// fib(5) = fib(4) + fib(3)
// fib(4) = fib(3) + fib(2)
// fib(3) = fib(2) + fib(1)
// fib(2) = 1
// fib(1) = 1

// cache

// fib(4) = 2 + 1           = 3
// fib(3) = 1 + 1           = 2
// fib(2) = 1
// fib(1) = 1
function fib(num) {
    // base case (kasus di mana rekursif berhenti/di-return)
    if (num === 2 || num === 1) {
        return 1
    }
    else return fib(num-1) + fib(num-2)
}

function a(b) {
    // bakal susa dibaca sama programmer baru
    (b === 2 || b === 1) ? 1 : a(b-1) + a(b-2)
}

// fibCache(5)
//     |       \
// fibCache(4)  fibCache(3) \
//     |       \               fibCache(2)  fibCache(1)
// fibCache(3)  fibCache(2)
//     |           
// fibCache(2)  fibCache(1)

cache = {
    3: 2,
    4: 3
}
// fibCache(5) -> 5
//     |       \
//    3         2 
//     |                     
//    2  1
//     |           
//    2  1

function fibCache(num, cache={}) {
    // cache di javascript -> object
    // kalo bahasa lain -> HashMap/HashTable
    let _cache = cache
    if (_cache[num] !== undefined) {
        return _cache[num]
    }
    if (num === 2 || num === 1) {
        return 1
    }
    let res = fibCache(num-1, _cache) + fibCache(num-2, _cache)
    _cache[num] = res
    return res
}
// console.log({1: 13}[2])
let t0 = new Date().getTime()
console.log(fibCache(45))
let t1 = new Date().getTime()

console.log(`${t1-t0}ms`)

const library = (a, b) => {
    return a + 5 + b
}

console.log(handlerMath(library, 2, 3))