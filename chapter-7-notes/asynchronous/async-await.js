const promiseParam = (tepatin, ingkarin, password) => {
    (password === "123456") ? tepatin('Password bener') : ingkarin('password salah');
}

const checkPassword = (password) => {
    return new Promise((tepatin, ingkarin) => promiseParam(tepatin, ingkarin, password))
}

function suit(player, comp) {
    // kalo sama, draw
    if (player === comp) return "draw"

    // kalo beda, ...
    else if (player === "gunting") {

        //  normal if-else
        if (comp === "kertas") return "player 1"
        else if (comp !== "kertas") return "computer"

        // ternary operator
        // return (comp === "kertas") ? "player 1" : "computer"
    }
    else if (player === "batu") {
        return comp === "kertas" ? "computer" : "player 1"
    }
    else if (player === "kertas") {
        return comp === "batu" ? "player 1" : "computer"
    }
}

// Resolve kalo ada yang menang/kalah
// Reject kalo seri
function suitPromise(player, comp) {
    return new Promise((resolve, reject) => {
        let result = suit(player, comp);
        result === "draw" ? reject(result) : resolve(result);
    })
}

async function mainPassword() {
    try {
        const result = await checkPassword("123456")
        console.log('try', result)
    }
    catch (err) {
        console.log('catch', err)
    }
}
async function mainSuit() {
    try {
        const result = await suitPromise('kertas', 'kertas')
        console.log('try', result)
    }
    catch (err) {
        console.log('catch', err)
    }
}

mainPassword()
mainSuit()