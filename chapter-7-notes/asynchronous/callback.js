function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

// const callMeAfter3Seconds = async (callback) => {
//     console.log("tidur")
//     callback.onSleep()
//     await sleep(3000);
//     console.log("bangun")
//     callback.onAwake()
// }

const callMeAfter3Seconds = async ({ onSleep, onAwake }) => {
    console.log("tidur")
    onSleep()
    await sleep(3000);
    console.log("bangun")
    onAwake()
}

const myName = () => console.log('bagas')
const yourName = () => console.log('bagus')

console.log(
    callMeAfter3Seconds({
        onSleep: myName,
        onAwake: yourName
    })
)

document.getElementById('name').addEventListener('click', callMeAfter3Seconds)