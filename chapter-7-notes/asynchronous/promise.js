const promiseParam = (tepatin, ingkarin, password) => {
    (password === "123456") ? tepatin('Password bener') : ingkarin('password salah');
}

const checkPassword = (password) => {
    return new Promise((tepatin, ingkarin) => promiseParam(tepatin, ingkarin, password))
}

checkPassword('123456')
    .then(resolve => {
        console.log('masuk then')
        console.log(resolve)
    })
    .catch(reject => {
        console.log('masuk catch')
        console.error(reject)
    })

function suit(player, comp) {
    // kalo sama, draw
    if (player === comp) return "draw"

    // kalo beda, ...
    else if (player === "gunting") {

        //  normal if-else
        if (comp === "kertas") return "player 1"
        else if (comp !== "kertas") return "computer"

        // ternary operator
        // return (comp === "kertas") ? "player 1" : "computer"
    }
    else if (player === "batu") {
        return comp === "kertas" ? "computer" : "player 1"
    }
    else if (player === "kertas") {
        return comp === "batu" ? "player 1" : "computer"
    }
}

// Resolve kalo ada yang menang/kalah
// Reject kalo seri
function suitPromise(player, comp) {
    return new Promise((resolve, reject) => {
        let result = suit(player, comp);
        result === "draw" ? reject(result) : resolve(result);
    })
}

suitPromise('kertas', 'batu')
    .then(winner => {
        console.log('masuk then')
        console.log(`pemenangnya adalah ${winner}`)
        return new Promise((resolve, reject) => resolve('then 2'))
    })
    .then(hehe => {
        console.log('masuk then 2')
        console.log(hehe)
    })
    .catch(reject => {
        console.log('Seri! silakan coba lagi')
        console.error(reject)
    })