const upperCaseFirstLetterInWord = (word) => {
    let result = ''
    for (let i = 0; i < word.length; i++) {
        if (i === 0) {
            result += word[i].toUpperCase();
        } else {
            result += word[i];
        }
    }
    return result
}

const upperCaseFirstLetterEachWord = (sentence) => {
    return sentence                         // 'bagas kara ganteng'
        .split(' ')                         // ['bagas', 'kara', 'ganteng']
        .map(upperCaseFirstLetterInWord)    // ['Bagas', 'Kara', 'Ganteng']
        .join(' ')                          // 'Bagas Kara Ganteng'
}

console.log(upperCaseFirstLetterEachWord('bagas kara ganteng'))