export function suit(player, comp) {
    // kalo sama, draw
    if (player === comp) return "draw"

    // kalo beda, ...
    else if (player === "gunting") {

        //  normal if-else
        if (comp === "kertas") return "player 1"
        else if (comp !== "kertas") return "computer"

        // ternary operator
        // return (comp === "kertas") ? "player 1" : "computer"
    }
    else if (player === "batu") {
        return comp === "kertas" ? "computer" : "player 1"
    }
    else if (player === "kertas") {
        return comp === "batu" ? "player 1" : "computer"
    }
}

export default class SuitClass {
    suit(inputUser) {
        console.logreturn inputUser;
    }
}