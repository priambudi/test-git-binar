
var arraykuNumber = [1,2,3,4,5] // gausah pake var
let arraykuString = ['1','2','3']
const arraykuRandom = ['1',2,[1,2, {}]]


// key, value pair
let objectku = {
    key: 'value'
}

let arrayProfile = ["bagas", 21, ["gaming","tidur"]]

let nama = "reynald"
let objectProfile = {
    nama: {
        pendek: "Bagas",
        panjang: "Priambudi Lintang Bagaskara"
    },
    umur: 21,
    hobi: [
        "gaming",
        "tidur"
    ],
}

// nama pendek
// "Bagas"
console.log(nama)
console.log(objectProfile.nama['pendek'])
console.log(objectProfile['nama'].panjang)

// hobi 1 aja
// "tidur"
console.log(objectProfile.hobi) // masih array
console.log(objectProfile.hobi[1]) // ambil index ke-1