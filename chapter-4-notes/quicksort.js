function quicksort(array) {
    if (array.length <= 1) {
        return array;
    }

    var pivot = array[0];

    var left = [];
    var right = [];

    for (var i = 1; i < array.length; i++) {
        array[i] < pivot ? left.push(array[i]) : right.push(array[i]);
    }

    return quicksort(left).concat(pivot, quicksort(right));
};

// Time complexity linear
// length = 9
// 100ms
// length = 18
// 100ms

// Time complexity quadratic
// length = 9
// 100ms
// length = 18
// 10s

// Time complexity
// input array, diliat dari panjang array
// input string, diliat dari jumlah karakter

// Space complexity linear
// length = 9
// 3kb
// length = 18
// 6kb

// Space complexity quadratic
// length = 9
// 3kb
// length = 18
// 9kb

// Space complexity
// panjang array baru yang dibikin
// panjang string baru yang dibikin
// seberapa dalam recursive call?

// bisa nambahin space, untuk mempercepat time

// n = input size
// time = n log n
// space = log n

let array = [2, 4, 22, 5, 7, 3, 41, 1, 34]

console.log(quicksort(array))
console.log(array.sort())