// let a = 0;

// if (a === 0){
//     console.log('nol')
// } else if (a === 1){
//     console.log('satu')
// } else if (a === 2){
//     console.log('dua')
// } else if (a === 3){
//     console.log('tiga')
// } else {
//     console.log('au')
// }

// // apa yang mau kita cek? -> a
// switch(a) {
//     // misalnya a === 0
//     case 0:
//         console.log('nol')
        
//         // jangan lompat ke case berikutnya
//         break;

//     case 1:
//         console.log('satu')
//         break;
//     case 2:
//         console.log('dua')
//         break;
//     case 3:
//         console.log('tiga')
//         break;

//     // opsional, kalo ga masuk semua case
//     default:
//         console.log('au')
// }

// // apa yang mau kita cek? -> (a === 1 || a === 3)
// switch(a === 1 || a === 3) {
//     // misalnya (a === 1 || a === 3) === true
//     case true:
//         console.log('nol')
        
//         // jangan lompat ke case berikutnya
//         break;

//     case false:
//         console.log('satu')
//         break;
//     default:
//         console.log('au')
// }

// // apa yang mau kita cek? -> typeof a
// switch(typeof a) {
//     // misalnya typeof a === "string"
//     case "string":
//         console.log('nol')
        
//         // jangan lompat ke case berikutnya
//         break;

//     case "number":
//         console.log('satu')
//         break;
//     default:
//         console.log('au')
// }


let b = 0;

// if (b * 2 === 0){
//     console.log('nol')
// } if (b * 2 === 1){
//     console.log('satu')
// } else {
//     console.log('au')
// }
// console.log('')
// if (b * 2 === 0){
//     console.log('nol')
// } else if (b * 3 === 0){
//     console.log('satu')
// } else {
//     console.log('au')
// }

// switch(b === 1) { // -> true
//     case 0: // -> 0
//         console.log('nol')
//         break;
//     case 0: // -> 0
//         console.log('satu')
//         break;
//     case 1: // -> 0
//         console.log('dua')
//         break;
//     case false:
//         console.log('false')
//         break
//     default:
//         console.log('au')
// }

// 0 === 1 -> false
// cek case === false


let c = 10;

if (c * 2 - 5 === 15){
    c = 15
} else if (c / 3 === 5){
    c = 10
}

switch(c) { // -> true
    case 5:
        console.log('lima')
        break;
    case 10: // -> 0
        console.log('sepuluh')
        break;
    case 15: // -> 0
        console.log('lima belas')
        break;
    case 20:
        console.log('dua puluh')
        break
    default:
        console.log('au')
}