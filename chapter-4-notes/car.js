// class Van {
//     static NUMBER_OF_WHEELS = 4;
//     constructor() {
//         this.color = "oren";
//         this.name = "van";
//         this.numberOfSeats =  8
//     }
// }

// class Audi {
//     static NUMBER_OF_WHEELS = 4;
//     constructor() {
//         this.color = "ijo";
//         this.name = "audi";
//         this.numberOfSeats =  4
//     }
// }

// class Ferrari {
//     static NUMBER_OF_WHEELS = 4;
//     constructor() {
//         this.color = "kuning";
//         this.name = "ferrari";
//         this.numberOfSeats =  2
//     }
// }

// let van1 = new Van()
// console.log(van1.name)


// abstract, buat template class child yang lain
class Car {

    // static property
    static NUMBER_OF_WHEELS = 4;

    // instance property
    fuel = "pertamax"

    // constructor
    constructor(color, name, numberOfSeats) {
        if (this.constructor === Car) {
            throw new Error("Cannot instantiate from Abstract Class") 
            // Because it's abstract
        }
       
        this.color = color;
        this.name = name;
        this.numberOfSeats =  numberOfSeats
    }

    // method (public)
    drive() {
        // masih gatau nih mau implemen apa
    }

    _brake() {
        console.log("PSHHHHHHH")
    }
}

class Bus extends Car {

    // static property
    static NUMBER_OF_WHEELS = 6;

    // instance property
    fuel = "solar"

    // constructor
    constructor(props) {
        super(props)
        this.kenekName = props.kenekName
    }

    drive() {
        console.log("MERCI VROOOOM")
    }

    brake() {
        super._brake()
    }
}

class Bike extends Car {

    // static property
    static NUMBER_OF_WHEELS = 6;

    // instance property
    fuel = "solar"

    // constructor
    constructor(props) {
        super(props)
        this.kenekName = props.kenekName
    }

    _ring() {
        console.log("ringgg")
    }

// dipanggil di mana mana tapi di class Bike aja
    #privateMethod = () => {
        console.log(1)
        console.log(1)
        console.log(1)
        console.log(1)
    }

    method1(){
        let privateMethod = function() {

        }
        this.#privateMethod()
        this.method2()
    }
    method2(){
        this.#privateMethod()
    }
    method3(){
        this.#privateMethod()
    }
    method4(){
        this.#privateMethod()
    }
    method5(){
        this.#privateMethod()
    }
}

class Motorcycle extends Bike {

    // static property
    static NUMBER_OF_WHEELS = 6;

    // instance property
    fuel = "solar"

    // constructor
    constructor(props) {
        super(props)
    }

    ring() {
        super._ring()
    }
}

// let van = new Car("oren", "van", 8)
let merci = new Bus("putih", "white horse", 51, "bagas")
let polygon = new Bike("putih", "polygon", 2,"bagas")
let yamaha = new Motorcycle({color: "putih", name: "yamaha", numberOfSeats: 2})
// van.drive()
merci.drive()
merci.brake()
// polygon.brake()
polygon.drive()

// PROTECTED! GABOLEH DIPANGGIL
// polygon._ring()

yamaha._ring()
// van.drivePrivate()
// console.log(van.color, van.name, van.numberOfSeats)
// console.log(audi.color, audi.name, audi.numberOfSeats)
// console.log(ferrari.color, ferrari.name, ferrari.numberOfSeats)