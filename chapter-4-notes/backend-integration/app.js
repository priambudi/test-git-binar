//app.js
const http = require("http");
var url = require('url');

const PORT = process.env.PORT || 5000;


// 127.0.0.1:5000/?player1=batu&computer=batu
// seri

function suit(inputUser, inputComputer) {
    let result;
    if (inputUser == "gunting") {
        if (inputComputer == inputUser) {
            result = "seri";
        } else if (inputComputer == "kertas") {
            result = "menang"
        } else if (inputComputer == "batu") {
            result = "kalah"
        }
    } else if (inputUser == "batu") {
        if (inputComputer == inputUser) {
            result = "seri";
        } else if (inputComputer == "kertas") {
            result = "kalah"
        } else if (inputComputer == "gunting") {
            result = "menang"
        }
    } else if (inputUser == "kertas") {
        if (inputComputer == inputUser) {
            result = "seri";
        } else if (inputComputer == "gunting") {
            result = "kalah"
        } else if (inputComputer == "batu") {
            result = "menang"
        }
    }
    return result
}

const server = http.createServer(async (req, res) => {
    var queryData = url.parse(req.url, true).query;
    let player = queryData.player1;
    let computer = queryData.computer;
    let data = `https://${req.headers.host}/?player1=batu&computer=batu`;
    if (player && computer) {
        data = suit(player, computer)
    }
    res.writeHead(200, { "Content-Type": "application/json" });
    res.end(JSON.stringify(data));
});

server.listen(PORT, () => {
    console.log(`server started on port: ${PORT}`);
});
