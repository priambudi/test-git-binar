let nama = "bagas"

if (true) {
    let nama = "bukan bagas"
    console.log("inside block scope: ",nama)
}
console.log("outside block scope: ",nama)

let umur = 21

function getUmur(){
    let umur = 30
    console.log("inside block scope: ",umur)
}
console.log("outside block scope: ",umur)
getUmur()