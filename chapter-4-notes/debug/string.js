let str = "gambar-suit"
let expectedStrActive = "gambar-suit active"
let expectedStrInactive = "gambar-suit"

let toggleStr = function(){
    // TODO

    // kalo str ada active
    let isStrContainsActive = str.includes("active")
    if (isStrContainsActive) {
        // ilangin activenya
        str = str.replace("active","");

        // apus spasi
        str = str.replace(/\s/g, '');
    }

    // kalo str ga ada active
    else {
        // tambah active
        str += " active"
    }
}

console.log(str, 1)
// str = "gambar-suit"

toggleStr()

console.log(str, 2)
// str = "gambar-suit active"

toggleStr()

console.log(str, 3)
// str = "gambar-suit"