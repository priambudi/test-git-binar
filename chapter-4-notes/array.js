let myArray = [5,4,3,2,1]
console.log("push")
console.log(myArray.length)
console.log(myArray.push(0))
console.log(myArray.length)

console.log("")

console.log("pop")
console.log(myArray.length, myArray)
console.log(myArray.pop())
console.log(myArray.length, myArray)

console.log("")

console.log("shift")
console.log(myArray.length, myArray)
console.log(myArray.shift())
console.log(myArray.length, myArray)

console.log("")

console.log("unshift")
console.log(myArray.length, myArray)
console.log(myArray.unshift(100))
console.log(myArray.length, myArray)

console.log("")

function isEven(x) {
    return x % 2 === 0 // input 2, return true
}

// function mystery(y) {
//     if (isEven(y)) {
//         return y +1
//     } else {
//         return y
//     }
// }
// isEven(2) // === true
console.log("filter yang genep aja")
console.log(myArray.length, myArray)
myArray = myArray.filter(function(value){
    return isEven(value)
})
console.log(myArray)
console.log(myArray.length, myArray)

console.log("")

console.log("map, dikali 2 semua")
console.log(myArray.length, myArray)
myArray = myArray.map(function(value){
    return value * 2
})
console.log(myArray)
console.log(myArray.length, myArray)