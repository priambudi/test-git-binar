/**
 * @param {number[]} digits
 * @return {number[]}
 */
var plusOne = function (digits) {
    let str = ""
    for (let i = 0; i < digits.length; i++) {
        // console.log(digits[i])
        str += digits[i]
    }
    str = parseInt(str) + 1
    str = str + ""
    let arrayStr = str.split("")
    let result = arrayStr.map(function (value) {
        return parseInt(value)
    })
    return result
};

/**
 * @param {number[]} digits
 * @return {number[]}
 */
function plusOneV2(digits) {

    // loop dari elemen terakhir, ke elemen pertama
    for (let i = digits.length - 1; i >= 0; i--)

        // jika elemen terakhir adalah 9
        if (digits[i] == 9 && i == digits.length - 1) {
            // elemen sebelum terakhir += 1
            digits[i - 1]++;

            // elemen terakhir dijadiin 0
            digits[i] = 0

        }

        // elemen terakhir += 1
        else if (i == digits.length - 1) digits[i]++

    return digits

}

console.log(plusOneV2([1, 2, 9]))
console.log(plusOneV2([9, 9, 9]))