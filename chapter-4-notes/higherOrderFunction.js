// Functional Programming: Higher Order Function (HOF)
// HOF = fungsi yang menerima/mengembalikan sebuah fungsi

const strArray = ['JavaScript', 'Java', 'C'];

/**
 * 
 * @param {*} array : [] number
 * @param {*} callback : ()=>{} function
 * @returns array
 */

function forEach(array, callback) {
    const newArray = [];
    for (let i = 0; i < array.length; i++) {
        let hasilFungsiCallback = callback(array[i]);
        console.log(`i: ${i}`)
        console.log(`array[i]: ${array[i]}`)
        console.log(`hasilFungsiCallback: ${hasilFungsiCallback}`)
        console.log(`newArray before push: ${newArray}`)
        newArray.push(hasilFungsiCallback);
        console.log(`newArray after push: ${newArray}`)
        console.log('')
    }
    return newArray;
}

const getStringLength = (item) => {
    return item.length;
}

const lenArray = forEach(strArray, getStringLength);

// const lenArray2 = strArray.forEach((item) => {
//     console.log(item)
//     return item.length;
// })

// console.log(lenArray2)
console.log(lenArray);