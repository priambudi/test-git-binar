console.log(1 == 1) // true
console.log(1 === 1) // true

console.log(1 == '1') // true
console.log(1 === '1') // false

console.log('')

console.log(1 != 1) // false
console.log(1 !== 1) // false

console.log(1 != '1') // false
console.log(1 !== '1') // true

console.log('')

// LOGICAL OPERATORS
// && -> AND operator
console.log(true && true) // true
console.log(true && false) // false
console.log(false && true) // false
console.log(false && false) // false

console.log('')

// || -> OR operator
console.log(true || true) // true
console.log(true || false) // true
console.log(false || true) // true
console.log(false || false) // false
