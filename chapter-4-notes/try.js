class Human {
    constructor(name, address) {
        this.name = name;
        this.address = address;
    }
    #doGossip = () => {
        console.log(`My address will become viral ${this.address}`)
    }
    talk() {
        console.log(this.#doGossip()); // Call the private method
    }
    static #isHidingArea = true;
}

let mj = new Human("Michael Jackson", "Isekai");
console.log(mj.talk()) // Will run, won't return error!
// Output: My address will become viral Isekai

// https://www.w3schools.com/js/tryit.asp?filename=tryjs_try_catch
try {
    Human.#isHidingArea // Will return an error!
    mj.#doGossip() // Won't run, will return error!
}
catch (err) {
    console.log(1)
    console.error(err)
}
