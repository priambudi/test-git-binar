function test(arg1, arg2) {
    return [arg1, arg2] // array, index 0 = arg1, index 1 = arg2
}

function test2(...args) {
    return args[9]
}

function test3(angka1, angka2 = 0, angka3 = 0) {
    return angka1 + angka2 + angka3
}

console.log(test(1,2))
console.log(test2(1,2,3,4,5,6,7,8,9,10))
console.log(test3(4,null,6))
console.log(test3(4))